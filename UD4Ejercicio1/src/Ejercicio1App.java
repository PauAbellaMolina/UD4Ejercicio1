public class Ejercicio1App {
	public static void main(String[] args) {
		final int A = 10;
		final int B = 30;
		
		//Suma:
		System.out.print("Suma: ");
		System.out.println(A+B);
		
		//Resta
		System.out.print("Resta: ");
		System.out.println(A-B);
		
		//Multiplicación
		System.out.print("Multiplicación: ");
		System.out.println(A*B);
		
		//División
		System.out.print("División: ");
		System.out.println(A/B);
		
		//Módulo
		System.out.print("Módulo: ");
		System.out.println(A%B);
	}
}